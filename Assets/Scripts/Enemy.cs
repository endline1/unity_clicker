using UnityEngine;
using UnityEngine.EventSystems;

public class Enemy : MonoBehaviour, IPointerClickHandler
{
    private int health;
    private int maxHealth;
    [SerializeField] private Transform healthbar;
    private float maxHealthbar;

    protected float speed;

    protected GameHandler gameHandler;

    [SerializeField] private GameObject deathEffect;

    private void Start() 
    {
        gameHandler = GameObject.FindObjectOfType<GameHandler>();
        
        health = gameHandler.commonEnemyHealth;
        maxHealth = health;
        maxHealthbar = healthbar.localScale.x;

        speed = gameHandler.commonEnemySpeed;

        InheritantStart();
    }

    protected virtual void InheritantStart() 
    {
        // это старт, но для наследника этого скрипта, чтобы местный старт выполнялся (вероятно есть более изящное решение)
    }

    private void Update() 
    {
        // это нужно для того, чтобы можно было наследовать от этого класса при этом меняя паттерн поведения врагов
        Move();
    }

    public void OnPointerClick(PointerEventData eventData) 
    {
        TakeDamage();

        if (health <= 0) Die();
    }

    // в этом методе описывается движение каждого типа монстров
    protected virtual void Move() 
    {
        // логика движения
    }

    private void TakeDamage()
    {
        health--;
        healthbar.localScale = new Vector3(maxHealthbar / maxHealth * health, healthbar.localScale.y, healthbar.localScale.z);
    }

    // метод публичный для бустера, который убивает всех монстров на экране
    public void Die()
    {
        // доавляем очки
        GameObject.FindObjectOfType<GameHandler>().AddScore(1);
        // эффект при смерти монстра
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        // уничтожаем монстра
        Destroy(gameObject);
    }
}
