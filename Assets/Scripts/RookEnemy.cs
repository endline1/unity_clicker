using UnityEngine;

public class RookEnemy : Enemy
{
    private Vector3[] patrolPoints = new Vector3[2];
    private float borderDistance;

    private Vector3 target = new Vector3();

    protected override void InheritantStart()
    {
        borderDistance = gameHandler.spawnBordersLength;
        
        // выбор случайного направения движения
        if (Random.value > 0.5f) 
        {
            patrolPoints[0] = new Vector3(transform.position.x, transform.position.y, -borderDistance);
            patrolPoints[1] = new Vector3(transform.position.x, transform.position.y, borderDistance);
        }
        else
        {
            patrolPoints[0] = new Vector3(-borderDistance, transform.position.y, transform.position.z);
            patrolPoints[1] = new Vector3(borderDistance, transform.position.y, transform.position.z);
        }

        target = patrolPoints[Random.Range(0, 2)];
    }
    
    protected override void Move() 
    {
        if (transform.position == patrolPoints[0])
        {
            target = patrolPoints[1];
        }
        else if (transform.position == patrolPoints[1]) 
        {
            target = patrolPoints[0];
        }
        
        // Монстр не прыгает, не падает, так что можно не нагружать тут всё физикой
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        transform.LookAt(target);
    }
}
