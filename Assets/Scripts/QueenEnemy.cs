using UnityEngine;

public class QueenEnemy : Enemy
{
    private float borderDistance;
    private Vector3 target = new Vector3();

    protected override void InheritantStart() 
    {
        borderDistance = gameHandler.spawnBordersLength;
        
        target = RandomPosition();
    }

    protected override void Move() 
    {
        if (transform.position == target) 
        {
            target = RandomPosition();
        }
        
        // Монстр не прыгает, не падает, так что можно не нагружать тут всё физикой
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        transform.LookAt(target);
    }

    private Vector3 RandomPosition() 
    {
        return new Vector3(Random.Range(-borderDistance, borderDistance), transform.position.y,
                           Random.Range(-borderDistance, borderDistance));
    }
}
