using UnityEngine;
using System.IO;
using TMPro;

public class ScoresTable : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreboard;

    void OnEnable() 
    {
        string table = "";
        Directory.CreateDirectory(Application.persistentDataPath + "/Scores");
        string[] files = Directory.GetFiles(Application.persistentDataPath + "/Scores");
        System.Array.Reverse(files);
        foreach (string filePath in files) 
        {
            string entry = File.ReadAllText(filePath);
            ScoreEntry score = JsonUtility.FromJson<ScoreEntry>(entry);

            table += "\n[" + score.time + "] " + score.score;
        }       
        scoreboard.text = table;
    }
}
