using UnityEngine;

public class KillerBooster : Booster
{
    protected override void MakeEffect() 
    {
        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy")) 
        {
            enemy.GetComponent<Enemy>().Die();
        }
    }
}
