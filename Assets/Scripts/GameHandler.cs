using System.IO; // для работы с файлами (сохранение рекордов)
using UnityEngine;
using TMPro;

public class GameHandler : MonoBehaviour
{
    [SerializeField] private GameObject[] enemyVariants;

    public float spawnBordersLength;

    [SerializeField] private float spawnDelay;
    [SerializeField] private float spawnDelayOffset;
    [System.NonSerialized] public float spawnTimer;

    private int playerScore = 0;
    [SerializeField] private TextMeshProUGUI scoreText;

    [SerializeField] private GameObject gameOverPanel;
    [SerializeField] private GameObject newRecordText;

    [SerializeField] private const float DIFFICULTY_MULTIPLIER = 1.1f; 

    public int commonEnemyHealth;
    [SerializeField] private float secretEnemyHealth;

    public float commonEnemySpeed;

    private void Update()
    {
        if (spawnTimer <= 0)
        {
            // сброс таймера с учётом случайности 
            spawnTimer = spawnDelay + Random.Range(-spawnDelayOffset, spawnDelayOffset);

            // вычисление случайной позиции в пределах карты
            Vector3 randomPosition = new Vector3(Random.Range(-spawnBordersLength, spawnBordersLength), 0,
                                                 Random.Range(-spawnBordersLength, spawnBordersLength));

            // спавн случайного монстра
            Instantiate(enemyVariants[Random.Range(0, enemyVariants.Length)], randomPosition, Quaternion.identity);
        
            // после спавна нового монстра проверяем общее количество
            if (GameObject.FindGameObjectsWithTag("Enemy").Length >= 10) 
            {
                GameLose();
                AddScoreEntry();
            }
        } 
        else 
        {
            spawnTimer -= Time.deltaTime;
        }
    }

    public void AddScore(int value) 
    {
        playerScore += value;
        scoreText.text = playerScore.ToString();

        // сложность повышается каждые 10 очков
        if (playerScore % 10 == 0)
        {
            RaiseDifficulty();
        }
    }

    private void RaiseDifficulty() 
    {
        spawnDelay /= DIFFICULTY_MULTIPLIER;
        spawnDelayOffset /= DIFFICULTY_MULTIPLIER;
        secretEnemyHealth *= DIFFICULTY_MULTIPLIER;
        commonEnemyHealth = Mathf.RoundToInt(secretEnemyHealth);
        commonEnemySpeed *= DIFFICULTY_MULTIPLIER;
    }

    private void GameLose() 
    {
        gameOverPanel.SetActive(true);
    }

    private void AddScoreEntry() 
    {
        int lastHighestScore = 0;

        /* перебираем все json'ы чтобы найти файл с наибольшим рекордом
        (должен быть более красивый способ, но я не так часто работаю с сохранениями) */
        Directory.CreateDirectory(Application.persistentDataPath + "/Scores");
        foreach (string filePath in Directory.GetFiles(Application.persistentDataPath + "/Scores")) 
        {
            string entry = File.ReadAllText(filePath);
            ScoreEntry score = JsonUtility.FromJson<ScoreEntry>(entry);

            if (score.score > lastHighestScore) 
            {
                lastHighestScore = score.score;
            }
        }

        if (playerScore > lastHighestScore) 
        {
            newRecordText.SetActive(true);
        
            // добавляем запись о новом рекорде
            ScoreEntry score = new ScoreEntry();
            score.score = playerScore;
            score.time = System.DateTime.Now.ToString();            
            string entry = JsonUtility.ToJson(score);
            File.WriteAllText(Application.persistentDataPath + "/Scores/Score_" + playerScore + ".json", entry);
        }
    }
}

[System.Serializable]
public class ScoreEntry {
    public int score;
    public string time;
}