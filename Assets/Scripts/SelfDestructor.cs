using UnityEngine;

public class SelfDestructor : MonoBehaviour
{
    [SerializeField] private float timer;

    void Start()
    {
        Invoke("Die", timer); // invoke, кажется, отвратительное решение для такой ситуации, но пока так
    }

    private void Die() 
    {
        Destroy(gameObject);
    }
}
