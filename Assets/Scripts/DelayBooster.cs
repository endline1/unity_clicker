using UnityEngine;

public class DelayBooster : Booster
{
    protected override void MakeEffect() 
    {
        GameObject.FindObjectOfType<GameHandler>().spawnTimer = 3.0f; // 3 секунды
    }
}
