using UnityEngine;
using UnityEngine.EventSystems;

public class Booster : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject soundEffect;
    
    public void OnPointerClick(PointerEventData eventData) 
    {
        MakeEffect();
        Instantiate(soundEffect);
        Destroy(gameObject);
    }

    protected virtual void MakeEffect() 
    {
        // эффект от бустера здесь
    }
}
