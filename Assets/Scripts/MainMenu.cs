using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{    
    // метод, сменяющий сцену на игровую
    public void NewGame() 
    {
        SceneManager.LoadScene(1);
    }

    // показать/скрыть панель
    public void ShowHidePanel(GameObject panel)
    {   
        panel.SetActive(!panel.activeSelf);
    }

    // выйти из игры
    public void QuitGame() 
    {
        Application.Quit();
    }
}
